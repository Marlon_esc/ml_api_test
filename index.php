<?php


// Define minimum supported PHP version
define('PHP', '7.1.3');
define('LARAVEL_START', microtime(true));

// Check PHP version
if (version_compare(PHP_VERSION, PHP, '<')) {
    die('Versión de PHP; ' . PHP_VERSION . ', versión requerida'. PHP);
}

// Register the auto-loader
require __DIR__.'/vendor/autoload.php';

// Load the app
$app = require_once(__DIR__.'/bootstrap/app.php');

// Run the app
$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);
