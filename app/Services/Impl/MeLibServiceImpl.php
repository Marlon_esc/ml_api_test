<?php

namespace App\Services\Impl;

use App\Exceptions\GenericException;

use Carbon\Carbon;
use App\Services\BaseService;
use App\Services\MeLibService;
use App\Models\Token;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Excel;

class MeLibServiceImpl implements BaseService, MeLibService{

    private $client_id;
    private $client_secret;
    private static $instance = null;
    const BASE_URI           = 'https://api.mercadolibre.com';
    const REDIRECT_URI       = 'https://webhook.site/3ce53d26-0e47-48c5-b37b-4ed3569f8d57';


    public function __construct(){

        $this->client_id        = env('MERCADO_LIBRE_CLIENT_ID');
        $this->client_secret    = env('MERCADO_LIBRE_CLIENT_SECRET');

    }

    public static function getInstance(){
        if (self::$instance == null){
            self::$instance = new MeLibServiceImpl();
        }
        return self::$instance;
    }

    public function getToken(){

        $client = new \GuzzleHttp\Client(['base_uri' => self::BASE_URI]);

        $response = $client->request('POST', '/oauth/token', [
            'form_params' => [
                "grant_type"        => "authorization_code",
                "client_id"         => $this->client_id,
                "client_secret"     => $this->client_secret,
                'code'              => 'TG-600ba1e7e8dfc50006cea5e3-126814491', #por el momento lo ponemos manualmente
                "redirect_uri"      => self::REDIRECT_URI
            ]
        ]);

        $token = json_decode($response->getBody()->getContents(), true);

        Token::updateOrInsert(
            ['id' => 1],
            ['access_token' => json_encode($token)]
        );

        return json_decode(Token::find(1)->access_token);

    }

    public function refreshToken(){

        $_token = json_decode(Token::find(1)->access_token);

        $client = new \GuzzleHttp\Client(['base_uri' => self::BASE_URI]);

        $response = $client->request('POST', '/oauth/token', [
            'form_params' => [
                "grant_type"        => "refresh_token",
                "client_id"         => $this->client_id,
                "client_secret"     => $this->client_secret,
                'refresh_token'     => $_token->refresh_token
            ]
        ]);

        $token = json_decode($response->getBody()->getContents(), true);

        Token::updateOrInsert(
            ['id' => 1],
            ['access_token' => json_encode($token)]
        );

        return json_decode(Token::find(1)->access_token);

    }

    public function searchItem($query){

        try {

            $_token = json_decode(Token::find(1)->access_token);

            $client = new \GuzzleHttp\Client(['base_uri' => self::BASE_URI]);

            $response = $client->request('GET', '/sites/MLM/search?q='. $query, [
                "headers" => [
                    "Authorization" => "Bearer $_token->access_token"
                ]
            ]);

            $result_search = json_decode($response->getBody());

            $average = $this->getAveragePriceOfPublications($result_search);

            $quantity = $this->getQuantityOfProductsSale($result_search);

            $quantityOfProductsSold = $this->getQuantityOfProductsSold30Days($result_search);

            $prices30Days = $this->getSalesPriceLast30Days($result_search);

            return [
                    'average' => $average,
                    'quantity' => $quantity,
                    'prices30Days' => $prices30Days,
                    'quantityOfProductsSold' => $quantityOfProductsSold
            ];

        } catch (\Exception $e) {

            if( $e->getCode() == 401 ) {

                $this->refreshToken();

            }
        }


    }

    private function getAveragePriceOfPublications($result_search){

        $price_sum = 0;

        foreach ($result_search->results as $item) {

            $price_sum += $item->price;

        }

        return number_format($price_sum / $result_search->paging->limit, 2);

    }

    private function getQuantityOfProductsSale($result_search){

        $available_quantity = 0;

        foreach ($result_search->results as $item) {

            if($item->available_quantity > 0){

                $available_quantity += $item->available_quantity;

            }

        }

        return $available_quantity;
    }

    private function getQuantityOfProductsSold30Days($result_search){

        $array_dates = $this->getLast30Days();

        $products30Days = [];

        foreach ($result_search->results as $item) {

            foreach ($item->prices->prices as $price) {

                if($price->type == "standard"){

                    $date = new Carbon($price->last_updated);

                    $key = array_search($date->format('d-m-Y') , $array_dates);

                    if($key){
                       $products30Days[$key] = [ $array_dates[$key] => $item->sold_quantity ];
                    }

                    krsort($products30Days);

                }
            }

        }

        return $products30Days;
    }

    private function getSalesPriceLast30Days($result_search){

        $array_dates = $this->getLast30Days();

        $prices30Days = [];

        foreach ($result_search->results as $item) {

            foreach ($item->prices->prices as $price) {

                if($price->type == "standard"){

                    $date = new Carbon($price->last_updated);

                    $key = array_search($date->format('d-m-Y') , $array_dates);

                    if($key){
                       $prices30Days[$key] = [ $array_dates[$key] => $price->amount ];
                    }

                    krsort($prices30Days);

                }
            }


        }


        return $prices30Days;
    }

    private function getLast30Days(){

        $dates = [];

        $a= 0;

        while ($a  < 30) {

            $dates[] = Carbon::now()->subDays($a)->format('d-m-Y') ;

            $a++;
        }

        return $dates;
    }



}

?>
