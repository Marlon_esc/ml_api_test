<?php

namespace App\Services;

interface MeLibService {

    public function getToken();

    public function refreshToken();

    public function searchItem($query);

}

?>
