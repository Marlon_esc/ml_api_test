<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Token extends Model
{
    use SoftDeletes;

    protected $table    = 'token';
    protected $hidden   = ['created_at', 'updated_at'];
    protected $dates    = ['deleted_at'];
    protected $guarded  = ['id'];
}
