<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\HttpResponseJSON;

class HomeController extends Controller
{
    use HttpResponseJSON;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }


}
