<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\HttpResponseJSON;
use App\Services\Impl\MeLibServiceImpl;
use Illuminate\Support\Facades\Log;
use Exception;

class MeLibController extends Controller
{
    use HttpResponseJSON;

    public function __construct(){

        $this->meLibService = MeLibServiceImpl::getInstance();

    }

    public function searchItem(Request $request){

        try {

            $items = [
                "items" => $this->meLibService->searchItem($request->get('query'))
            ];

            return $this->success('success', $items);

        } catch (Exception $e) {

            Log::error($e);

            return $this->error($e->getMessage());
        }

    }
}
