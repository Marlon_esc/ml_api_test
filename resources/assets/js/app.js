const { default: Axios } = require('axios');

require('./bootstrap');

window.Vue = require('vue');

var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };

import html2canvas from 'html2canvas';

const app = new Vue({
    el: '#kt_content',
    data() {
        return {
            show:  true,
            search: '',
            average: 0,
            quantity: 0,
            dates: [],
            prices30Days: [],
            quantityOfProductsSold: []
        }
    },
    mounted(){
        this.chart();
        this.chart2();
    },
    methods: {

        onKeySearch(){
            console.log(this.search)

            let url = "/api/v1/searchItem";

            Axios.post(url, { query: this.search })
                .then(response => {

                    let items = response.data.data.items;

                    this.average = items.average;

                    this.quantity = items.quantity;


                    Object.keys(items.prices30Days).map( key => {

                        this.dates.push(Object.keys(items.prices30Days[key])[0]);

                        this.prices30Days.push(items.prices30Days[key][Object.keys(items.prices30Days[key])[0]])

                    });

                    Object.keys(items.quantityOfProductsSold).map( key => {

                        this.quantityOfProductsSold.push(items.quantityOfProductsSold[key][Object.keys(items.quantityOfProductsSold[key])[0]]);

                    });


                    ApexCharts.exec('mychart', 'updateOptions', {
                        xaxis: {
                            categories: Object.values(this.dates)
                        }
                    }, false, true);

                    ApexCharts.exec('mychart2', 'updateOptions', {
                        xaxis: {
                            categories: Object.values(this.dates)
                        }
                    }, false, true);

                    ApexCharts.exec('mychart', 'updateSeries', [{
                        data: this.prices30Days
                      }], true);

                    ApexCharts.exec('mychart2', 'updateSeries', [{
                    data: this.quantityOfProductsSold
                    }], true);

                })
                .catch(error => {

                    console.log(error);

                });
        },
        chart(){
                var element = document.getElementById("kt_charts_widget_6_chart");

                if (!element) {
                    return;
                }

                var options = {
                    series: [{
                        name: 'Precio de ventas en los últimos 30 días',
                        type: 'bar',
                        stacked: true,
                        data: [0]
                    }],
                    chart: {
                        id: 'mychart',
                        stacked: true,
                        height: 350,
                        toolbar: {
                            show: false
                        }
                    },
                    plotOptions: {
                        bar: {
                            stacked: true,
                            horizontal: false,
                            endingShape: 'rounded',
                            columnWidth: ['12%']
                        },
                    },
                    legend: {
                        show: false
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'smooth',
                        show: true,
                        width: 2,
                        colors: ['transparent']
                    },
                    xaxis: {
                        categories: [0],
                        axisBorder: {
                            show: false,
                        },
                        axisTicks: {
                            show: false
                        },
                        labels: {
                            style: {
                                colors: KTAppSettings['colors']['gray']['gray-500'],
                                fontSize: '12px',
                                fontFamily: KTAppSettings['font-family']
                            }
                        }
                    },
                    yaxis: {
                        labels: {
                            style: {
                                colors: KTAppSettings['colors']['gray']['gray-500'],
                                fontSize: '12px',
                                fontFamily: KTAppSettings['font-family']
                            }
                        }
                    },
                    fill: {
                        opacity: 1
                    },
                    states: {
                        normal: {
                            filter: {
                                type: 'none',
                                value: 0
                            }
                        },
                        hover: {
                            filter: {
                                type: 'none',
                                value: 0
                            }
                        },
                        active: {
                            allowMultipleDataPointsSelection: false,
                            filter: {
                                type: 'none',
                                value: 0
                            }
                        }
                    },
                    tooltip: {
                        style: {
                            fontSize: '12px',
                            fontFamily: KTAppSettings['font-family']
                        },
                        y: {
                            formatter: function (val) {
                                return "$" + val
                            }
                        }
                    },
                    colors: [KTAppSettings['colors']['theme']['base']['info'], KTAppSettings['colors']['theme']['base']['primary'], KTAppSettings['colors']['theme']['light']['primary']],
                    grid: {
                        borderColor: KTAppSettings['colors']['gray']['gray-200'],
                        strokeDashArray: 4,
                        yaxis: {
                            lines: {
                                show: true
                            }
                        },
                        padding: {
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0
                        }
                    }
                };

                var chart = new ApexCharts(element, options);
                chart.render();



        },
        chart2(){

            let element = document.getElementById("quantityOfProductsSold");

            if (!element) {
                return;
            }

            let options = {
                series: [{
                    name: 'Cantidad de productos vendidos en los últimos 30 días',
                    type: 'bar',
                    stacked: true,
                    data: [0]
                }],
                chart: {
                    id: 'mychart2',
                    stacked: true,
                    height: 350,
                    toolbar: {
                        show: false
                    }
                },
                plotOptions: {
                    bar: {
                        stacked: true,
                        horizontal: false,
                        endingShape: 'rounded',
                        columnWidth: ['12%']
                    },
                },
                legend: {
                    show: false
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    curve: 'smooth',
                    show: true,
                    width: 2,
                    colors: ['transparent']
                },
                xaxis: {
                    categories: [0],
                    axisBorder: {
                        show: false,
                    },
                    axisTicks: {
                        show: false
                    },
                    labels: {
                        style: {
                            colors: KTAppSettings['colors']['gray']['gray-500'],
                            fontSize: '12px',
                            fontFamily: KTAppSettings['font-family']
                        }
                    }
                },
                yaxis: {
                    labels: {
                        style: {
                            colors: KTAppSettings['colors']['gray']['gray-500'],
                            fontSize: '12px',
                            fontFamily: KTAppSettings['font-family']
                        }
                    }
                },
                fill: {
                    opacity: 1
                },
                states: {
                    normal: {
                        filter: {
                            type: 'none',
                            value: 0
                        }
                    },
                    hover: {
                        filter: {
                            type: 'none',
                            value: 0
                        }
                    },
                    active: {
                        allowMultipleDataPointsSelection: false,
                        filter: {
                            type: 'none',
                            value: 0
                        }
                    }
                },
                tooltip: {
                    style: {
                        fontSize: '12px',
                        fontFamily: KTAppSettings['font-family']
                    },
                    y: {
                        formatter: function (val) {
                            return  val
                        }
                    }
                },
                colors: [ KTAppSettings['colors']['theme']['light']['primary']],
                grid: {
                    borderColor: KTAppSettings['colors']['gray']['gray-200'],
                    strokeDashArray: 4,
                    yaxis: {
                        lines: {
                            show: true
                        }
                    },
                    padding: {
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0
                    }
                }
            };

            let chart = new ApexCharts(element, options);
            chart.render();



    },
        async snapShot(){

            this.show = !this.show;

            let element = document.querySelector("#kt_content");
            await html2canvas(element,{
                backgroundColor: "#ffffff",
                x: 0,
                y: element.offsetTop * -0.9,
                width: element.scrollWidth,
                height: element.scrollHeight * 1.2,
                scrollY: 0,
                scrollX: 0,
                logging: true,
                removeContainer: true,
                scale: 2,
                onclone: function(document) {
                    document.body.scrollTop = 0;
                    let statistics = document.querySelector("#kt_content");
                    statistics.style.position = 'absolute';
                    statistics.style.top = 0;
                    statistics.style.width = '98em';
                }
            }).then(canvas => {

                var link = document.createElement('a');

                link.href = canvas.toDataURL();

                link.download = 'captura.png';

                document.body.appendChild(link);

                link.click();

                document.body.removeChild(link);

                this.show = !this.show;
            });
        }

    }
});
