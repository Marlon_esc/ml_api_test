<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.partials.head')


<body id="kt_body" class="quick-panel-right demo-panel-right offcanvas-right header-fixed header-mobile-fixed subheader-enabled aside-enabled aside-static page-loading">

    <div class="d-flex flex-column flex-root">

        <div class="d-flex flex-row flex-column-fluid page">

            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

                @include('layouts.partials.header')

                @yield('content')

                @include('layouts.partials.footer')

            </div>

        </div>

    </div>

    @include('layouts.partials.scrolltop')

    @include('layouts.partials.scripts')

</body>

</html>
