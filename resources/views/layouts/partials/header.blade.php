<div id="kt_header" class="header header-fixed">

    <div class="container d-flex align-items-stretch justify-content-between">

        <div class="d-none d-lg-flex align-items-center mr-3">

            <a href="index.html">
                <img alt="Logo" src="{{ asset('public/ml.png') }}" class="logo-sticky max-h-60px" />
            </a>

        </div>

    </div>
</div>
