 <!--begin::Global Theme Bundle(used by all pages)-->

 <script src="{{asset('public/assets/plugins/global/plugins.bundle.js')}}"></script>
 <script src="{{asset('public/assets/plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
 <script src="{{asset('public/assets/js/scripts.bundle.js')}}"></script>

 <script src="{{ asset('public/js/app.js') }}" ></script>
 <!--end::Global Theme Bundle-->
@yield('scripts')
