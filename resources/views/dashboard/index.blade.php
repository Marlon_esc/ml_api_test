@extends('layouts.app')

@section('content')

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
        <div
            class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">
                        Buscador de productos Mercado libre
                    </h2>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center flex-wrap">
                <!--begin::Button-->
                <a href="#" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" v-on:click="snapShot" v-show="show">
                    <span class="d-none d-md-inline">Capturar pantalla</span>
                </a>


            </div>

        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">

        <div class="container">

            <div class="alert alert-custom alert-white alert-shadow fade show gutter-b" role="alert">

                <div class="alert-text">

                    <div class="quick-search quick-search-inline w-auto" id="kt_quick_search_inline">

                        @include('dashboard.partials.form-search')

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    @include('dashboard.partials.chart')
                </div>

            </div>

        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

@endsection

@section('scripts')

@endsection
